package eu.realmcompany.realmconstruct.statics;

import net.minecraft.server.v1_16_R1.*;
import org.bukkit.craftbukkit.v1_16_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class WorldStatics {

    /**
     * Spawn hologram
     *
     * @param player         Player to be shown to
     * @param position       Position of the hologram
     * @param text           Text of the hologram
     * @param secondsToDeath self-explanatory, set to -1 to never destroy entity
     * @return EntityArmorStand
     */
    public static EntityArmorStand showHologram(@NotNull Player player, @NotNull Vector position, @NotNull String text, long secondsToDeath) {
        return showHologram(player, position, text, secondsToDeath, TimeUnit.SECONDS);
    }
    /**
     * Spawn hologram
     *
     * @param player         Player to be shown to
     * @param position       Position of the hologram
     * @param text           Text of the hologram
     * @param timeToDeath    self-explanatory, set to -1 to never destroy entity
     * @param unit           Time unit
     * @return EntityArmorStand
     */
    public static EntityArmorStand showHologram(@NotNull Player player, @NotNull Vector position, @NotNull String text, long timeToDeath, TimeUnit unit) {
        WorldServer worldServer = ((CraftWorld) player.getWorld()).getHandle();
        EntityArmorStand hologram = EntityTypes.ARMOR_STAND.create(worldServer);
        hologram.setSmall(true);
        hologram.setInvisible(true);
        hologram.setNoGravity(true);
        hologram.setCustomName(IChatBaseComponent.ChatSerializer.jsonToComponent("{\"text\":\"" + text + "\"}"));
        hologram.setCustomNameVisible(true);
        hologram.setLocation(position.getX(), position.getY(), position.getZ(), 0f, 0f);

        PacketPlayOutSpawnEntityLiving entitySpawn = new PacketPlayOutSpawnEntityLiving(hologram);
        PacketPlayOutEntityMetadata entityMetadata = new PacketPlayOutEntityMetadata(hologram.getId(), hologram.getDataWatcher(), false);
        ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(entitySpawn);
        ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(entityMetadata);

        if (timeToDeath != -1)
            Executors.newScheduledThreadPool(1).schedule(() -> {
                PacketPlayOutEntityDestroy entityDestroy = new PacketPlayOutEntityDestroy(hologram.getId());
                ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(entityDestroy);
            }, timeToDeath, unit);
        return hologram;
    }

    /**
     * Highlights block at specified position for specified player.
     *
     * @param player         Player which will see the highlight
     * @param position       Position of highlight
     * @param secondsToDeath self-explanatory, set to -1 to never destroy entity
     * @return EntityShulker
     */
    public static EntityShulker highlightBlock(@NotNull Player player, @NotNull Vector position, long secondsToDeath) {
        WorldServer worldServer = ((CraftWorld) player.getWorld()).getHandle();
        EntityShulker highlight = EntityTypes.SHULKER.create(worldServer);
        highlight.setInvulnerable(true);
        highlight.setInvisible(true);
        highlight.setLocation(position.getX(), position.getY(), position.getZ(), 0f, 0f);
        CraftEntity craftHighlight = CraftLivingEntity.getEntity(worldServer.getServer(), highlight);
        craftHighlight.setGlowing(true);

        PacketPlayOutSpawnEntityLiving entitySpawn = new PacketPlayOutSpawnEntityLiving(highlight);
        PacketPlayOutEntityMetadata entityMetadata = new PacketPlayOutEntityMetadata(highlight.getId(), highlight.getDataWatcher(), false);
        ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(entitySpawn);
        ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(entityMetadata);


        if (secondsToDeath != -1)
            Executors.newScheduledThreadPool(1).schedule(() -> {
                PacketPlayOutEntityDestroy entityDestroy = new PacketPlayOutEntityDestroy(highlight.getId());
                ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(entityDestroy);
            }, secondsToDeath, TimeUnit.SECONDS);
        return highlight;
    }

}

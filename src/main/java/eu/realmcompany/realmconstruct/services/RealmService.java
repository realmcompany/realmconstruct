package eu.realmcompany.realmconstruct.services;

import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.abstraction.AComponent;
import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.services.chat.ChatService;
import eu.realmcompany.realmconstruct.services.justice.JusticeService;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class RealmService extends AComponent implements Listener {

    /**
     * Map of all services
     */
    private final Map<Class<? extends AService>, AService> services = new HashMap<>();

    /**
     * Default constructor
     *
     * @param plugin Instance of plugin
     */
    public RealmService(@NotNull BukkitPlugin plugin) {
        super(plugin);

        registerService(JusticeService.class, new JusticeService(this));
        registerService(ChatService.class, new ChatService(this));
    }

    @Override
    public void initialize() throws Exception {

        // Init all services
        this.services.forEach((clazz, instance) -> {
            long start = System.currentTimeMillis();
            getLogger().info("Initializing service §e%s", instance.getServiceName());

            boolean failed = false;
            try {
                instance.initialize();
            } catch (Exception e) {
                failed = true;
                getLogger().error("Failed initialization of service §e%s", e, instance.getServiceName());
                e.printStackTrace();
            }
            long elapsed = (System.currentTimeMillis() - start);
            getLogger().debug("(§e%s§7) Init took: §a%dms" + (failed ? " -- §cBut failed!" : ""), instance.getServiceName(), elapsed);
        });

        Bukkit.getPluginManager().registerEvents(this, this.getPlugin());
    }

    @Override
    public void terminate() throws Exception {

        // Terminate all services
        this.services.forEach((clazz, instance) -> {
            long start = System.currentTimeMillis();
            getLogger().info("Terminating service §e%s", instance.getServiceName());

            boolean failed = false;
            try {
                instance.terminate();
            } catch (Exception e) {
                failed = true;
                getLogger().error("Failed termination of service §e%s", e, instance.getServiceName());
                e.printStackTrace();
            }
            long elapsed = (System.currentTimeMillis() - start);
            getLogger().debug("(§e%s§7) Term took: §a%dms" + (failed ? " -- §cBut failed!" : ""), instance.getServiceName(), elapsed);
        });
    }


    /**
     * Registers service
     * @param serviceClass     Service class
     * @param serviceInstance  Service instance
     * @param <A>              Type of service
     */
    private <A extends AService> void registerService(@NotNull Class<A> serviceClass, @NotNull A serviceInstance) {
        getLogger().info("Registering service §e%s", serviceInstance.getServiceName());
        this.services.put(serviceClass, serviceInstance);
    }

    /**
     * @param serviceClass Service class
     * @param <A>          Type of service
     * @return Service instance
     */
    public <A extends AService> A getService(@NotNull Class<A> serviceClass) {
        AService selected = this.services.get(serviceClass);
        if (serviceClass.isInstance(selected))
            return (A) selected;
        return null;
    }
}

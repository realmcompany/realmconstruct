package eu.realmcompany.realmconstruct.services.auth;

import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.services.RealmService;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

@Getter
public class AuthorizationService extends AService {

    @NotNull
    private final AuthorizationHandlers handlers;
    @NotNull
    private final AuthorizationFactory dataRegistry;

    /**
     * Service constructor
     *
     * @param instance Service instance
     */
    public AuthorizationService(@NotNull RealmService instance) {
        super(instance);
        handlers = new AuthorizationHandlers(this);
        dataRegistry = new AuthorizationFactory(this);
    }


    @Override
    public void initialize() throws Exception {
        handlers.enable();
    }

    @Override
    public void terminate() throws Exception {

    }
}

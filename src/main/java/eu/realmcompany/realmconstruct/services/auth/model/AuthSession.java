package eu.realmcompany.realmconstruct.services.auth.model;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;

@Getter
public class AuthSession implements Serializable, Predicate<String> {
    @NotNull
    private final String name;
    @NotNull
    private final UUID uuid;

    @Nullable
    private final String secret;

    private boolean accepted = false;

    private final CompletableFuture<Player> loginCallback = new CompletableFuture<>();


    public AuthSession(@NotNull String name, @NotNull UUID uuid, @Nullable String secret) {
        this.name = name;
        this.uuid = uuid;
        this.secret = secret;
    }


    /**
     * Used to test password
     *
     * @param s raw password
     */
    @Override
    public boolean test(String s) {
        //TODO hash and salt
        return secret.equals(s);
    }

    /**
     * Used to login user & activate callbacks
     *
     * @param secret raw password
     */
    public boolean login(String secret) {
        if (test(secret)) {
            loginCallback.complete(Bukkit.getPlayer(uuid));
            return accepted = true;
        }
        return false;
    }


}

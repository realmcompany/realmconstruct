package eu.realmcompany.realmconstruct.services.auth;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

abstract class AuthorizationComponent {

    @NotNull
    @Getter
    protected AuthorizationService authorizationService;

    /**
     * Default constructor for a component
     *
     * @param authorizationService authorization service instance
     */
    protected AuthorizationComponent(@NotNull AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;
    }

    /**
     * Used to enable component
     *
     * @throws Exception when something goes wrong
     */
    public abstract void enable() throws Exception;


    /**
     * Used to disable component
     *
     * @throws Exception when something goes wrong
     */
    public void disable() throws Exception {
        //TODO: NOT IMPLEMENTED
    }


    /**
     * Used to reload component
     *
     * @throws Exception when something goes wrong
     */
    public void reload() throws Exception {
        disable();
        enable();
    }
}

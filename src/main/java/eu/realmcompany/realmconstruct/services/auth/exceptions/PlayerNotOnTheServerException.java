package eu.realmcompany.realmconstruct.services.auth.exceptions;

public class PlayerNotOnTheServerException extends Exception {
    public PlayerNotOnTheServerException(String message) {
        super(message);
    }
}

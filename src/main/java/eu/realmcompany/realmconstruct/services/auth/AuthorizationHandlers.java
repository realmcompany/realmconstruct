package eu.realmcompany.realmconstruct.services.auth;

import eu.realmcompany.realmconstruct.services.auth.model.AuthPacketGateway;
import eu.realmcompany.realmconstruct.services.auth.model.AuthSession;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R1.CraftServer;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class AuthorizationHandlers extends AuthorizationComponent implements Listener {


    /**
     * Default constructor for a component
     *
     * @param authorizationService authorization service instance
     */
    public AuthorizationHandlers(@NotNull AuthorizationService authorizationService) {
        super(authorizationService);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void playerJoin(@NotNull PlayerJoinEvent event) {
        /*try {
            AuthSession session = getAuthorizationService().getDataRegistry().makeSession(event.getPlayer()).get(30, TimeUnit.SECONDS);
            ((CraftPlayer) event.getPlayer()).getHandle().playerConnection.networkManager.setPacketListener(
                    new AuthPacketGateway(
                            ((CraftServer) Bukkit.getServer()).getServer(),
                            ((CraftPlayer) event.getPlayer()).getHandle().playerConnection.networkManager,
                            ((CraftPlayer) event.getPlayer()).getHandle(), session
                    )
            );
        } catch (TimeoutException | ExecutionException | InterruptedException ex) {
            event.getPlayer().kickPlayer(String.format("Failed to retrieve your Authorization session! %s", ex.getClass().getSimpleName()));
        }*/
    }

    @Override
    public void enable() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, getAuthorizationService().getInstance().getPlugin());
    }
}

package eu.realmcompany.realmconstruct.services.auth;

import eu.realmcompany.realmconstruct.services.auth.exceptions.PlayerNotOnTheServerException;
import eu.realmcompany.realmconstruct.services.auth.model.AuthSession;
import org.apache.commons.lang.NotImplementedException;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class AuthorizationFactory extends AuthorizationComponent {

    public AuthorizationFactory(@NotNull AuthorizationService authorizationService) {
        super(authorizationService);
    }

    @Override
    public void enable() throws Exception {
        throw new NotImplementedException();
    }


    public CompletableFuture<AuthSession> makeSession(@NotNull Player player) {
        return CompletableFuture.completedFuture(new AuthSession("WattMann", UUID.randomUUID(), "kokot"));
    }

    public CompletableFuture<AuthSession> makeSession(@NotNull String username) {
        Player context;
        if ((context = Bukkit.getPlayer(username)) == null)
            return CompletableFuture.failedFuture(new PlayerNotOnTheServerException(String.format("Player %s is not on the server!", username)));
        return makeSession(context);
    }

    public CompletableFuture<AuthSession> makeSession(@NotNull UUID id) {
        Player context;
        if ((context = Bukkit.getPlayer(id)) == null)
            return CompletableFuture.failedFuture(new PlayerNotOnTheServerException(String.format("Player %s is not on the server!", id)));
        return makeSession(context);
    }

}

package eu.realmcompany.realmconstruct.services.auth.model;

import net.minecraft.server.v1_16_R1.*;
import org.jetbrains.annotations.NotNull;


public class AuthPacketGateway extends PlayerConnection {

    @NotNull
    private final AuthSession authSession;

    public AuthPacketGateway(MinecraftServer minecraftserver, NetworkManager networkmanager, EntityPlayer entityplayer, @NotNull AuthSession authSession) {
        super(minecraftserver, networkmanager, entityplayer);
        this.authSession = authSession;
    }

    @Override
    public void a(PacketPlayInFlying packetplayinflying) {
        if (!authSession.isAccepted()) {
            syncPosition();
            return;
        }
        super.a(packetplayinflying);
    }

    @Override
    public void a(PacketPlayInBlockDig dig) {
        if (!authSession.isAccepted()) {
            PacketPlayOutBlockChange packet = new PacketPlayOutBlockChange(player.getWorld(), dig.b());
            //packet.block = player.getWorld().(dig.b());
            sendPacket(packet);
            return;
        }
        super.a(dig);
    }

    @Override
    public void a(PacketPlayInBlockPlace place) {
        if (!authSession.isAccepted()) {
            return;
        }
        super.a(place);
    }

    @Override
    public void a(PacketPlayInEntityAction packetplayinentityaction) {
        if (!authSession.isAccepted())
            return;
        super.a(packetplayinentityaction);
    }

    @Override
    public void a(PacketPlayInUseEntity packetplayinuseentity) {
        if (!authSession.isAccepted())
            return;
        super.a(packetplayinuseentity);
    }

    @Override
    public void a(PacketPlayInChat packetplayinchat) {
        authSession.login(packetplayinchat.b());
    }
}

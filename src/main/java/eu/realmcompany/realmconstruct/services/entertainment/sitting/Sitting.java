package eu.realmcompany.realmconstruct.services.entertainment.sitting;

import eu.realmcompany.realmconstruct.services.entertainment.EntertainmentService;
import lombok.Getter;
import eu.realmcompany.realmconstruct.abstraction.AService;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Sitting extends AService.Child<EntertainmentService> implements Listener  {

    @Getter
    private final List<UUID> entities = new ArrayList<>();

    public Sitting(@NotNull EntertainmentService parent) {
        super(parent);
    }

    @Override
    public void initialize() throws Exception {
        registerAsListener();
    }

    @Override
    public void terminate() throws Exception {
        entities.forEach(uuid -> {
            try {
                Bukkit.getEntity(uuid).remove();
            } catch (NullPointerException ignored) {
            }
        });
        entities.clear();
    }

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        Block b = e.getClickedBlock();
        if (b != null)
            if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK))
                if (e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR))
                    if (b.getType().name().contains("_STAIRS"))
                        sit(b, e.getPlayer());
    }

    @EventHandler
    public void dismount(EntityDismountEvent e) {
        if (e.getEntity() instanceof Player)
            if (entities.contains(e.getDismounted().getUniqueId())) {
                entities.remove(e.getDismounted().getUniqueId());
                e.getDismounted().remove();

                e.setCancelled(true);
                e.getEntity().setVelocity(new Vector(0, 0.5, 0));
            }

    }

    public void sit(@NotNull Block block, @NotNull Player player) {
        Stairs stairs = (Stairs) block.getBlockData();
        if (stairs.getHalf() == Bisected.Half.BOTTOM) {
            // gotta flip dat boy so he isnt facing goddamn wall
            int yawBase = 0;

            Vector direction = ((Stairs) block.getBlockData()).getFacing().getDirection();
            boolean isInner = stairs.getShape().name().contains("INNER");

            // We don't need this
            /*
            if (!isInner) {
                // get block faced by the stairs. if the area isnt empty... yeet that boy
                Block frontBlock = block.getWorld().getBlockAt(block.getLocation().add(direction.multiply(new Vector(-1, -1, -1))));

                if (!frontBlock.getType().equals(Material.AIR))
                    return;
            }
            */

            // get the top block, if the area isn't empty... yeet that boyo
            Block topBlock = block.getWorld().getBlockAt(block.getLocation().add(new Vector(0, 1, 0)));
            if (!topBlock.getType().equals(Material.AIR))
                return;


            if (direction.getZ() == 1)
                yawBase = -180;
            else if (direction.getZ() == -1)
                yawBase = 0;
            else if (direction.getX() == 1)
                yawBase = 90;
            else if (direction.getX() == -1)
                yawBase = -90;
            else if (direction.getX() == 1)
                yawBase = 90;
            else
                return;

            if (isInner)
                yawBase += 45;

            Vector center = block.getBoundingBox().getCenter();
            center.setY(center.getY() - 0.95D);

            Location loc = center.toLocation(player.getWorld());

            loc.setYaw(yawBase);
            ArmorStand armorStand = (ArmorStand) block.getWorld().spawnEntity(
                    loc, EntityType.ARMOR_STAND);

            armorStand.setSmall(true);
            armorStand.setGravity(false);
            armorStand.addPassenger(player);
            armorStand.setVisible(false);

            ((CraftPlayer) player).getHandle().setHeadRotation(yawBase);

            entities.add(armorStand.getUniqueId());
        }

    }

}

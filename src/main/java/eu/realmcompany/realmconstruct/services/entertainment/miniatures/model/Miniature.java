package eu.realmcompany.realmconstruct.services.entertainment.miniatures.model;

import eu.realmcompany.realmconstruct.services.entertainment.miniatures.MiniatureAPI;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_16_R1.Vec3D;
import org.bukkit.Material;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class Miniature {

    @Getter @NotNull
    private Map<Integer, Part> positions = new HashMap<>();

    @Getter @NotNull
    private Vector dimensions = new Vector(0d, 0d, 0d);

    @Getter
    private boolean small = false;

    private Miniature() {
    }

    /**
     * Represents single Part of Miniature.
     */
    public static class Part {

        @Getter @Setter
        private @NotNull Material material;
        @Getter @Setter
        private @NotNull Vector pos;

        @Getter @Setter
        private float yaw = 0f;
        @Getter @Setter
        private float pitch = 0f;

        public Part(@NotNull Material material, @NotNull Vector pos, float yaw, float pitch) {
            this.material = material;
            this.pos = pos;
            this.yaw = yaw;
            this.pitch = pitch;
        }

        public Part(@NotNull Material material, @NotNull Vector pos) {
            this.material = material;
            this.pos = pos;
        }
    }

    /**
     *
     */
    public static class Builder {

        @Getter
        private int lastIndex = 0;
        private @NotNull Miniature miniature = new Miniature();

        /**
         * Creates a builder with already existing Miniature
         * @param miniature Miniature
         * @return Builder
         */
        public static @NotNull Builder create(@NotNull Miniature miniature, @NotNull Vector dimensions, boolean small) {
            Builder builder = new Builder();
            builder.miniature = miniature;
            builder.miniature.dimensions = dimensions;
            builder.miniature.small      = small;
            return builder;
        }

        /**
         * Creates a builder
         * @return Builder
         */
        public static @NotNull Builder create(@NotNull Vector dimensions, boolean small) {
            return create(new Miniature(), dimensions, small);
        }

        /**
         * Adds part
         * @param material Material of block
         * @param pos      Relative position of block
         * @param yaw      Yaw rotation of a part
         * @param pitch    Pitch rotation of a part
         * @return Builder
         */
        public @NotNull Builder addPart(@NotNull Material material, @NotNull Vector pos, float yaw, float pitch) {
            this.miniature.positions.put(lastIndex++, new Part(
                    material,
                    MiniatureAPI.Math.translateRelativePos(pos, this.miniature.isSmall()),
                    yaw,
                    pitch));
            return this;
        }

        /**
         * Adds part
         * @param material Material of Block
         * @param pos      World position of Block
         * @return Builder
         */
        public @NotNull Builder addPart(@NotNull Material material, @NotNull Vector pos) {
            return addPart(material, pos, 0f, 0f);
        }

        public @NotNull Miniature get() {
            return miniature;
        }
    }
}

package eu.realmcompany.realmconstruct.services.entertainment.commands;

import org.bukkit.Particle;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

public class LoveCommand implements CommandExecutor {

    public LoveCommand() {
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        Player player = (Player) commandSender;
        Particle particle = Particle.HEART;

        player.getWorld().spawnParticle(particle, player.getLocation().add(new Vector(0, 2.2, 0)), 1);


        return true;
    }
}

package eu.realmcompany.realmconstruct.services.entertainment;

import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.services.RealmService;
import eu.realmcompany.realmconstruct.services.entertainment.miniatures.model.Miniature;
import eu.realmcompany.realmconstruct.services.entertainment.sitting.Sitting;
import eu.realmcompany.realmconstruct.statics.WorldStatics;
import net.minecraft.server.v1_16_R1.Vec3D;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.Random;

public class EntertainmentService extends AService {

    private Sitting sitting;

    public EntertainmentService(@NotNull RealmService instance) {
        super(instance);

        sitting = new Sitting(this);
    }

    @Override
    public void initialize() throws Exception {
        sitting.initialize();

        this.getInstance().getPlugin().getServer().getCommandMap().register("realmland", new Command("miniature") {
            @Override
            public boolean execute(@NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] args) {
                Vector origin = ((Player) commandSender).getLocation().toVector();


                Miniature.Builder builder = Miniature.Builder.create(new Vector(3, 3, 3), false);
                for(int x = 0; x < 3; x++) {
                    for(int y = 0; y < 3; y++) {
                        for(int z = 0; z < 3; z++) {
                            builder.addPart(Material.STONE, new Vector(x, y, z));
                        }
                    }
                }
                World world = ((Player) commandSender).getWorld();
                builder.get().getPositions().forEach((index, part) -> {
                    ArmorStand entity = (ArmorStand) world.spawnEntity(part.getPos().toLocation(world).add(origin), EntityType.ARMOR_STAND);
                    entity.setVisible(false);
                    entity.setGravity(false);
                    entity.setCanTick(false);

                    Bukkit.getScheduler().scheduleAsyncRepeatingTask(getPlugin(), () -> {
                        entity.setRotation(0, new Random().nextInt(90));
                    }, 20, 0);

                    entity.setItem(EquipmentSlot.HEAD, new ItemStack(Material.STONE));
                });


                return true;
            }
        });
    }

    @Override
    public void terminate() throws Exception {
        sitting.terminate();
    }
}

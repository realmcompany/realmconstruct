package eu.realmcompany.realmconstruct.services.entertainment.miniatures;

import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

public class MiniatureAPI {


    public static class Math {

        /**
         * Translates relative world position into relative part position
         * @param relWorldPos Position in real world
         * @param small    Whether the part is small or not.
         * @return Translated relative relWorldPos
         */
        public static @NotNull Vector translateRelativePos(@NotNull Vector relWorldPos, boolean small) {
            double factor;
            if(small)
                factor = 0.439d;
            else
                factor = 0.627d;

            Vector relPartPos = new Vector();
            relPartPos.setX(relWorldPos.getX() * factor);
            relPartPos.setY(relWorldPos.getY() * factor);
            relPartPos.setZ(relWorldPos.getZ() * factor);

            return relPartPos;
        }

    }


}

package eu.realmcompany.realmconstruct.services.chat.idiotikum;

import eu.realmcompany.realmconstruct.services.chat.ChatService;
import lombok.Getter;
import lombok.Setter;
import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.services.RealmService;
import net.minecraft.server.v1_16_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_16_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.IntStream;

public class LiveChat extends AService.Child<ChatService> implements Listener {

    @Getter @Setter
    private boolean enabled = false;

    @Getter
    private final Map<UUID, List<EntityArmorStand>> standData = new HashMap<>();

    public LiveChat(@NotNull ChatService parent) {
        super(parent);
    }

    @Override
    public void initialize() throws Exception {
        registerAsListener();

        this.getPlugin().getServer().getCommandMap().register("realmland", new Command("livechat", ", ", "", Arrays.asList("lc", "livchat")) {
            @Override
            public boolean execute(@NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] args) {
                setEnabled(!isEnabled());
                commandSender.sendMessage(isEnabled() ? "§aLiveChat enabled" : "§cLiveChat disabled");
                return true;
            }
        });
    }

    @Override
    public void terminate() throws Exception {
        standData.clear();
    }

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        if(enabled) {
            WorldServer world = ((CraftWorld) event.getPlayer().getWorld()).getHandle();
            Location location = event.getPlayer().getLocation();
            EntityArmorStand message = EntityTypes.ARMOR_STAND.create(world);

            // register
            List<EntityArmorStand> originData = this.standData.getOrDefault(event.getPlayer().getUniqueId(), new ArrayList<>());

            // fill with nulls, since i cant get size working properly
            List<EntityArmorStand> newData = new ArrayList<>();
            IntStream.range(0, originData.size()+1).forEach(consumer -> {newData.add(null);});

            // shift arraylist
            for(int i = 0; i < originData.size(); i++)
                newData.set(i+1, originData.get(i));
            newData.set(0, message);

            // move old ones up
            for(int i = 0; i < originData.size(); i++) {
                EntityArmorStand stand = originData.get(i);
                if(stand != null) {
                    PacketPlayOutEntity.PacketPlayOutRelEntityMove move =
                            new PacketPlayOutEntity.PacketPlayOutRelEntityMove(stand.getId(),
                                    (short) 0,
                                    (short) ((0.3 * 32) * 128),
                                    (short) 0,
                                    event.getPlayer().isOnGround());

                    Bukkit.getOnlinePlayers().forEach(player -> {
                        ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(move);
                    });
                }
            }

            this.standData.put(event.getPlayer().getUniqueId(), newData);

            // metadata
            message.setLocation(location.getX(), location.getY() + 1.2, location.getZ(), 0, 0);
            message.setInvisible(true);
            message.setInvulnerable(true);
            message.setCustomName(IChatBaseComponent.ChatSerializer.jsonToComponent("{\"text\":\"§7" + event.getMessage() + "\"}"));
            message.setCustomNameVisible(true);
            message.setSmall(true);
            message.setNoGravity(true);

            // spawn
            PacketPlayOutSpawnEntityLiving spawn = new PacketPlayOutSpawnEntityLiving(message);
            PacketPlayOutEntityMetadata metadata = new PacketPlayOutEntityMetadata(message.getId(), message.getDataWatcher(), false);


            Bukkit.getOnlinePlayers().forEach(player -> {
                ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(spawn);
                ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(metadata);
            });

            // destroy later
            Bukkit.getScheduler().runTaskLater(this.getInstance().getPlugin(), () -> {
                Bukkit.getOnlinePlayers().forEach(player -> {
                    PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy(message.getId());
                    ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(destroy);

                    // unregister
                    List<EntityArmorStand> data = this.standData.getOrDefault(event.getPlayer().getUniqueId(), new ArrayList<>());
                    data.set(data.indexOf(message), null);
                    boolean empty = data.stream().allMatch(Objects::isNull);
                    if(empty)
                        data.clear();
                    this.standData.put(event.getPlayer().getUniqueId(), data);
                });
            }, 4 * 20 + (event.getMessage().toCharArray().length));
        }
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent event) {
        if(enabled) {
            List<EntityArmorStand> data = this.standData.getOrDefault(event.getPlayer().getUniqueId(), new ArrayList<>());
            data.stream().filter(Objects::nonNull).forEach(stand -> {

                if (event.getFrom().distance(event.getTo()) <= 8) {
                    PacketPlayOutEntity.PacketPlayOutRelEntityMove move =
                            new PacketPlayOutEntity.PacketPlayOutRelEntityMove(stand.getId(),
                                    (short) ((event.getTo().getX() * 32 - event.getFrom().getX() * 32) * 128),
                                    (short) ((event.getTo().getY() * 32 - event.getFrom().getY() * 32) * 128),
                                    (short) ((event.getTo().getZ() * 32 - event.getFrom().getZ() * 32) * 128),
                                    event.getPlayer().isOnGround());

                    Bukkit.getOnlinePlayers().forEach(player -> {
                        ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(move);
                    });
                } else {
                    Location location = event.getPlayer().getLocation();
                    stand.setLocation(location.getX(), stand.getPositionVector().getY(), location.getZ(), 0, 0);
                    PacketPlayOutEntityTeleport teleport = new PacketPlayOutEntityTeleport(stand);

                    Bukkit.getOnlinePlayers().forEach(player -> {
                        ((CraftPlayer) player).getHandle().playerConnection.networkManager.sendPacket(teleport);
                    });
                }
            });
        }
    }

}

package eu.realmcompany.realmconstruct.services.chat;

import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.services.RealmService;
import eu.realmcompany.realmconstruct.services.chat.idiotikum.LiveChat;
import eu.realmcompany.realmconstruct.services.justice.JusticeService;
import lombok.Getter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.jetbrains.annotations.NotNull;

public class ChatService extends AService {

    @Getter
    private LiveChat liveChat;

    public ChatService(@NotNull RealmService instance) {
        super(instance);
        this.liveChat = new LiveChat(this);
    }

    @Override
    public void initialize() throws Exception {
        registerAsListener();

       this.liveChat.initialize();
    }

    @Override
    public void terminate() throws Exception {
        this.liveChat.terminate();
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        String message = event.getMessage();
        String format  = this.getInstance().getPlugin().getResources().getPluginConfiguration().getConfiguration().getString("services.chat-service.chat-format");



    }
}

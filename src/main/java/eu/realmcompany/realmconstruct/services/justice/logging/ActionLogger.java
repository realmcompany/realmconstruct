package eu.realmcompany.realmconstruct.services.justice.logging;

import eu.realmcompany.realmconstruct.services.justice.JusticeService;
import lombok.Getter;
import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.services.RealmService;
import eu.realmcompany.realmconstruct.services.justice.logging.model.BlockAction;
import eu.realmcompany.realmconstruct.services.justice.logging.model.ContainerAction;
import eu.realmcompany.realmconstruct.tuples.Triple;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.concurrent.Executors;

public class ActionLogger extends AService.Child<JusticeService> implements Listener {

    @Getter @Setter
    private boolean logBlocks = false;
    @Getter @Setter
    private boolean logContainer = false;

    @Getter
    private final Map<@NotNull Vector,
            @NotNull List<Triple<UUID, Long, BlockAction>>> logBlockRecords = new HashMap<>();

    @Getter
    private final Map<@NotNull Vector,
            @NotNull List<Triple<UUID, Long, ContainerAction>>> logContainerRecords = new HashMap<>();


    public ActionLogger(@NotNull JusticeService parent) {
        super(parent);
    }

    @Override
    public void initialize() throws Exception {
        Bukkit.getPluginManager().registerEvents(this, this.getInstance().getPlugin());
    }

    @Override
    public void terminate() throws Exception {

    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if(!logBlocks)
            return;
        Executors.newSingleThreadExecutor().submit(() -> {
            logBlock(event.getBlock().getLocation().toVector(), event.getPlayer(), BlockAction.DESTROY);
        });
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if(!logBlocks)
            return;
        Executors.newSingleThreadExecutor().submit(() -> {
            logBlock(event.getBlock().getLocation().toVector(), event.getPlayer(), BlockAction.PLACE);
        });
    }

    @EventHandler
    public void onItemMove(InventoryMoveItemEvent event) {
        if(!logContainer)
            return;

    }

    private synchronized void logBlock(@NotNull Vector position, @NotNull Player player, @NotNull BlockAction action) {
        List<Triple<UUID, Long, BlockAction>> data = this.logBlockRecords.getOrDefault(
                position,
                new ArrayList<>());

        data.add(Triple.of(player.getUniqueId(), System.currentTimeMillis(), action));
        this.logBlockRecords.put(position, data);
    }


    private synchronized void logContainer(@NotNull Vector position, @NotNull Player player, @NotNull ContainerAction action) {
        List<Triple<UUID, Long, ContainerAction>> data = this.logContainerRecords.getOrDefault(
                position,
                new ArrayList<>());

        data.add(Triple.of(player.getUniqueId(), System.currentTimeMillis(), action));
    }
}

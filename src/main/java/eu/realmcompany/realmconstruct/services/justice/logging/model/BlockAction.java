package eu.realmcompany.realmconstruct.services.justice.logging.model;

public enum BlockAction {
    DESTROY, PLACE
}

package eu.realmcompany.realmconstruct.services.justice.commands;

import lombok.Getter;
import eu.realmcompany.realmconstruct.services.justice.JusticeService;
import eu.realmcompany.realmconstruct.services.justice.logging.model.BlockAction;
import eu.realmcompany.realmconstruct.statics.WorldStatics;
import eu.realmcompany.realmconstruct.tuples.Triple;
import net.minecraft.server.v1_16_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_16_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class ActionLoggerCommand implements CommandExecutor  {

    @Getter
    private final JusticeService justiceService;

    public ActionLoggerCommand(@NotNull JusticeService justiceService) {
        this.justiceService = justiceService;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] args) {
        Player player = (Player) commandSender;
        WorldServer worldServer = ((CraftWorld) ((CraftPlayer) player).getWorld()).getHandle();

        boolean oldest = false;
        if(args.length > 0) {
            if(args[0].equalsIgnoreCase("--oldest"))
                oldest = true;
            if(args[0].equalsIgnoreCase("--clearCache")) {
                this.justiceService.getActionLogger().getLogBlockRecords().clear();
                this.justiceService.getActionLogger().getLogContainerRecords().clear();
                System.gc();
                commandSender.sendMessage("§c# §fCleared cache");
                return true;
            }
            if(args[0].equalsIgnoreCase("--disable")) {
                this.justiceService.getActionLogger().setLogBlocks(false);
                this.justiceService.getActionLogger().setLogContainer(false);
                commandSender.sendMessage("§c# §fLogger disabled");
                return true;
            }
            if(args[0].equalsIgnoreCase("--enable")) {
                this.justiceService.getActionLogger().setLogBlocks(true);
                this.justiceService.getActionLogger().setLogContainer(true);
                commandSender.sendMessage("§a# §fLogger enabled");
                return true;
            }
        }

        Block targeted = player.getTargetBlock(10);
        if(targeted == null) {
            commandSender.sendMessage("§cpoint.at.block");
            return true;
        }

        Location realLocation = targeted.getLocation();
        Vector location = realLocation.toVector().add(new Vector(0.5, 0.0, 0.5));
        List<Triple<UUID, Long, BlockAction>> data =
                this.justiceService.getActionLogger().getLogBlockRecords().getOrDefault(realLocation.toVector(), new ArrayList<>());

        if(data.size() == 0) {
            WorldStatics.highlightBlock(player, location, 5);
            WorldStatics.showHologram(player, location.add(new Vector(0.0, 1.0, 0.0)), "§cNo data for this boyo!", 5);
            return true;
        }

        WorldStatics.highlightBlock(player, location, 5);

        Vector hologramLoc = location.clone();
        hologramLoc.add(new Vector(0.0, 1.0, 0.0));
        if(!oldest)
            for (int i = 0; i < data.size(); i++)
                showEntry(data, i, hologramLoc, player, 0.3 * i);
         else
            for(int i = data.size()-1; i >= 0; i--)
                showEntry(data, i, hologramLoc, player, (0.3 * i));


        return true;
    }

    private void showEntry(List<Triple<UUID, Long, BlockAction>> data, int i, Vector hologramLoc, Player player, double elevation) {
        Triple<UUID, Long, BlockAction> entry = data.get(i);
        Vector relative = hologramLoc.clone();
        relative.add(new Vector(0.0, elevation, 0.0));

        WorldStatics.showHologram(player, relative,
                "§7[" + new SimpleDateFormat("HH:mm:ss").format(new Date(entry.getSecond())) + "] §f"
                        + Bukkit.getOfflinePlayer(entry.getFirst()).getName() + ": §6" + entry.getThird().name(), 10);
    }
}

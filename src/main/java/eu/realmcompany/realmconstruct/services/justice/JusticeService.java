package eu.realmcompany.realmconstruct.services.justice;

import lombok.Getter;
import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.services.RealmService;
import eu.realmcompany.realmconstruct.services.justice.commands.ActionLoggerCommand;
import eu.realmcompany.realmconstruct.services.justice.logging.ActionLogger;
import org.jetbrains.annotations.NotNull;

public class JusticeService extends AService {

    @Getter
    private ActionLogger actionLogger;

    public JusticeService(@NotNull RealmService instance) {
        super(instance);

        actionLogger = new ActionLogger(this);
    }

    @Override
    public void initialize() throws Exception {
        this.getInstance().getPlugin().getCommand("actionlog").setExecutor(new ActionLoggerCommand(this));
        this.actionLogger.initialize();
    }

    @Override
    public void terminate() throws Exception {
        this.actionLogger.terminate();
    }
}

package eu.realmcompany.realmconstruct.services.justice.logging.model;

public enum ContainerAction {
    TAKE_ITEM, PUT_ITEM
}

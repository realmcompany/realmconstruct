package eu.realmcompany.realmconstruct.services.comfort;

import eu.realmcompany.realmconstruct.abstraction.AService;
import eu.realmcompany.realmconstruct.mcdev.network.PktStatics;
import eu.realmcompany.realmconstruct.services.RealmService;
import net.minecraft.server.v1_16_R1.ChatMessageType;
import net.minecraft.server.v1_16_R1.IChatBaseComponent;
import net.minecraft.server.v1_16_R1.PacketPlayOutChat;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;


public class ComfortService extends AService {

    public ComfortService(@NotNull RealmService instance) {
        super(instance);

    }

    @Override
    public void initialize() throws Exception {
        registerAsListener();
    }

    @Override
    public void terminate() throws Exception {

    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        PktStatics.setRealmConnection(event.getPlayer());
        event.setJoinMessage("§a• §fPripojil sa §e" + event.getPlayer().getName());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        event.setQuitMessage("§c• §fOdpojil sa §e" + event.getPlayer().getName());
    }


    @EventHandler
    public void onDeath(PlayerDeathEvent event) {
        Location location = event.getEntity().getLocation();
        this.getInstance().getLogger().info("Player '§e%s§r' died at §c%d §a%d §9%d", event.getEntity().getName(), location.getBlockX(), location.getBlockY(), location.getBlockZ());
        ((CraftPlayer) event.getEntity()).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(IChatBaseComponent.ChatSerializer.jsonToComponent(String.format("{\"text\":\"§8# §fZomrel si na %d %d %d (Klikni pre zobrazenie prikazu)\",\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/tp %1$d %2$d %3$d\"}}", location.getBlockX(), location.getBlockY(), location.getBlockZ())), ChatMessageType.SYSTEM, UUID.randomUUID()));

        event.setDeathMessage("§c☠ §f" + event.getDeathMessage());
    }
}

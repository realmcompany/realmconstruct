package eu.realmcompany.realmconstruct.mcdev.math;

import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

public class Mth {

    /**
     * Converts polar coordinates to 2D cartesian coordinates
     *
     * @param magnitude   Magnitude
     * @param azimuth Angle in degrees
     * @return Vector with X and Z
     */
    public static Vector polarToCartesian(double magnitude, double azimuth) {
        return new Vector((magnitude * Math.cos(Math.toRadians(azimuth))), 0, (magnitude * Math.sin(Math.toRadians(azimuth))));
    }


    /**
     * Calculates spherical coordinates from cartesian coordinates.
     * @param source Source position
     * @param destination Destination position
     *
     * @return Vector x being radius, y being yaw, and z being pitch
     * */
    public static @NotNull Vector cartesianToSpherical(@NotNull Vector source, @NotNull Vector destination) {
        Vector local = destination.clone().subtract(source);
        System.out.println(local);
        double radius = Math.sqrt(Math.pow(local.getX(), 2) + Math.pow(local.getY(), 2) + Math.pow(local.getZ(), 2));
        double azimuth = Math.toDegrees(Math.atan(local.getZ() / local.getX()));
        double inclination = Math.toDegrees(Math.acos(local.getY() / radius));

        double yaw;

        if(local.getX() > local.getZ())
            yaw = azimuth - 90;
        else yaw = azimuth + 90;

        return new Vector(radius, yaw, degToPitch(inclination));
    }

    /**
     * Converts spherical coordinates to cartesian coordinates
     *
     * @param magnitude   Magnitude
     * @param azimuth azimuth angle in degrees
     * @param incline incline angle in degrees
     * @return Vector with X, Y and Z
     */
    public static Vector sphericalToCartesian(double magnitude, double azimuth, double incline) {
        return new Vector(
                (magnitude * (Math.sin(Math.toRadians(incline)) * Math.cos(Math.toRadians(azimuth)))),
                magnitude * Math.cos(Math.toRadians(incline)),
                (magnitude * (Math.sin(Math.toRadians(incline)) * Math.sin(Math.toRadians(azimuth)))));
    }


    /**
     * Converts yaw angle to degree angle
     *
     * @param yaw Yaw
     * @return Degree
     */
    public static double yawToDeg(double yaw) {
        double deg = yaw + 90;
        if (deg >= 360)
            deg -= 360;
        return deg;
    }

    /**
     * Converts degree angle to yaw angle
     *
     * @param deg Degree
     * @return Pitch
     */
    public static double degToYaw(double deg) {
        return deg - 90;
    }

    /**
     * Converts pitch angle to degree angle
     *
     * @param pitch Pitch
     * @return Degree
     */
    public static double pitchToDeg(double pitch) {
        return pitch + 90;
    }

    /**
     * Converts degree angle to pitch angle
     *
     * @param deg Degree
     * @return Pitch
     */
    public static double degToPitch(double deg) {
        return deg - 90;
    }

    /**
     * Rounds number with offset
     *
     * @param num           Number to round
     * @param decimalOffset Offset
     * @return Rounded number
     */
    public static double round(double num, int decimalOffset) {
        double rounded = Math.round(num * (decimalOffset * 10));
        return rounded / (decimalOffset * 10);
    }


    /**
     * Converts RGB color to int
     * @param red   Red
     * @param green Green
     * @param blue  Blue
     * @return Int
     */
    public static int rgbToInt(int red, int green, int blue) {
        int result = 0b00000000_00000000_00000000;
        result = result | red   << 16;
        result = result | green << 8;
        result = result | blue;
        return result;
    }

}

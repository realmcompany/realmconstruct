package eu.realmcompany.realmconstruct.mcdev.network.server;

import eu.realmcompany.realmconstruct.mcdev.network.client.RealmNetworkManager;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.ReadTimeoutHandler;
import lombok.Getter;
import net.minecraft.server.v1_16_R1.*;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Queue;

public class RealmServerConnection {

    @Getter
    private final MinecraftServer server;
    @Getter
    private final ServerConnection nativeConnection;

    @Getter
    private List<ChannelFuture> listeningChannels;
    @Getter
    private List<NetworkManager> connectedChannels;

    @Getter
    private java.util.Queue<NetworkManager> pendingChannels;

    @Getter
    private InetAddress serverAddress;

    @Getter
    private int port;

    /**
     * Default constructor
     *
     * @param server Server instance
     */
    public RealmServerConnection(@NotNull MinecraftServer server) {
        this.server = server;
        this.nativeConnection = server.getServerConnection();
        inject();

        if (!this.server.getServerIp().isEmpty()) {
            try {
                serverAddress = InetAddress.getByName(this.server.getServerIp());
            } catch (UnknownHostException e) {
                System.err.println("Failed to bind realm server address");
                return;
            }
        }
        this.port = server.getPort();

        Class<? extends ServerChannel> channelClass;
        LazyInitVar<? extends MultithreadEventLoopGroup> eventGroup;

        if (Epoll.isAvailable() && server.k()) {
            channelClass = EpollServerSocketChannel.class;
            eventGroup = ServerConnection.b;
        } else {
            channelClass = NioServerSocketChannel.class;
            eventGroup = ServerConnection.a;
        }

        // close previous listener
        this.listeningChannels.forEach(channel -> {
            channel.channel().disconnect();
        });
        this.listeningChannels.clear();

        // sexy listener right here
        this.listeningChannels.add(new ServerBootstrap().channel(channelClass).childHandler(new ChannelInitializer<Channel>() {
            @Override
            protected void initChannel(Channel channel) throws Exception {
                try {
                    channel.config().setOption(ChannelOption.TCP_NODELAY, true);
                } catch (ChannelException channelexception) {
                    ;
                }
                channel.pipeline().addLast("timeout", new ReadTimeoutHandler(30))
                        .addLast("legacy_query", new LegacyPingHandler(nativeConnection))
                        .addLast("splitter", new PacketSplitter())
                        .addLast("decoder", new PacketDecoder(EnumProtocolDirection.SERVERBOUND))
                        .addLast("prepender", new PacketPrepender())
                        .addLast("encoder", new PacketEncoder(EnumProtocolDirection.CLIENTBOUND));

                NetworkManager networkmanager = new RealmNetworkManager(EnumProtocolDirection.SERVERBOUND);
                pendingChannels.add(networkmanager);

                channel.pipeline().addLast("packet_handler", networkmanager);
                networkmanager.setPacketListener(new HandshakeListener(server, networkmanager));
            }
        }).group(eventGroup.a())
                .localAddress(this.serverAddress, this.port)
                .option(ChannelOption.AUTO_READ, false)
                .bind().syncUninterruptibly());
    }

    /**
     * Injects into native ServerConnection
     */
    private void inject() {
        try {
            Field field = nativeConnection.getClass().getDeclaredField("listeningChannels");
            field.setAccessible(true);
            listeningChannels = (List<ChannelFuture>) field.get(nativeConnection);
            System.out.println("§aSuccessfully located and accessed listening channels from server connection");
        } catch (NoSuchFieldException e) {
            System.out.println("§cFailed to locate listening channels from server connection");
        } catch (IllegalAccessException e) {
            System.out.println("§cFailed to access listening channels from server connection");
        }

        try {
            Field field = nativeConnection.getClass().getDeclaredField("connectedChannels");
            field.setAccessible(true);
            connectedChannels = (List<NetworkManager>) field.get(nativeConnection);
            System.out.println("§aSuccessfully located and accessed connected channels from server connection");
        } catch (NoSuchFieldException e) {
            System.out.println("§cFailed to locate connected channels from server connection");
        } catch (IllegalAccessException e) {
            System.out.println("§cFailed to access connected channels from server connection");
        }

        try {
            Field field = nativeConnection.getClass().getDeclaredField("pending");
            field.setAccessible(true);
            pendingChannels = (Queue<NetworkManager>) field.get(nativeConnection);
            System.out.println("§aSuccessfully located and accessed pending channels   from server connection");
        } catch (NoSuchFieldException e) {
            System.out.println("§cFailed to locate connected pending channels from server connection");
        } catch (IllegalAccessException e) {
            System.out.println("§cFailed to access connected pending channels from server connection");
        }
    }

    /**
     * Allows auto-reading of listener
     */
    public void acceptConnections() {
        synchronized (this.listeningChannels) {
            for (ChannelFuture future : this.listeningChannels) {
                future.channel().config().setAutoRead(true);
            }
        }
    }

    /**
     * Denies auto-reading of listener
     */
    public void denyConnections() {
        synchronized (this.listeningChannels) {
            for (ChannelFuture future : this.listeningChannels) {
                future.channel().config().setAutoRead(true);
            }
        }
    }

}

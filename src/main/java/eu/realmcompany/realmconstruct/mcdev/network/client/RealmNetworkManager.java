package eu.realmcompany.realmconstruct.mcdev.network.client;

import io.netty.channel.ChannelHandlerContext;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.minecraft.server.v1_16_R1.*;

import javax.annotation.Nullable;
import java.util.UUID;

public class RealmNetworkManager extends NetworkManager {

    /**
     * Default nms constructor
     * @param protocolDirection Direction of communication
     */
    public RealmNetworkManager(EnumProtocolDirection protocolDirection) {
        super(protocolDirection);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelhandlercontext, Packet<?> packet) throws Exception {
        super.channelRead0(channelhandlercontext, packet);
    }

    @Override
    public void sendPacket(Packet<?> packet) {
        super.sendPacket(packet);
    }

    @Override
    public void sendPacket(Packet<?> packet, @Nullable GenericFutureListener<? extends Future<? super Void>> genericfuturelistener) {
        super.sendPacket(packet, genericfuturelistener);
    }
}

package eu.realmcompany.realmconstruct.mcdev.network.client;

import com.google.common.base.Charsets;
import lombok.Getter;
import net.minecraft.server.v1_16_R1.*;
import org.bukkit.Bukkit;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class RealmPlayerConnection extends PlayerConnection {

    @Getter
    private boolean isModded = false;

    public Map<Class<Packet<PacketListenerPlayIn>>, Consumer<Packet<PacketListenerPlayIn>>> callbacks = new HashMap<>();

    /**
     * Default nms constructor
     *
     * @param server         Server instance
     * @param networkManager NetworkManager instance
     * @param entityPlayer   EntityPlayer   instance
     */
    public RealmPlayerConnection(@NotNull MinecraftServer server, @NotNull NetworkManager networkManager, @NotNull EntityPlayer entityPlayer) {
        super(server, networkManager, entityPlayer);
    }


    @Override
    public void a(PacketPlayInFlying packetplayinflying) {
        super.a(packetplayinflying);
    }

    @Override
    public void a(PacketPlayInCustomPayload packet) {
        if(packet.tag.equals(PacketPlayInCustomPayload.a)) {
            String brand = packet.data.readUTF(255);
            if (!brand.equals("vanilla")) {
                System.out.println("Player " + getPlayer().getName() + " has client: " + brand);
                isModded = true;

                Bukkit.getOnlinePlayers().forEach(player -> {
                    player.sendMessage("§7Hráč " + this.getPlayer().getName() + " sa pripojil s módovaným klientom.");
                });
            }
        } else if(packet.tag.equals(new MinecraftKey("register"))) {
            System.out.println("Client " + getPlayer().getName() + " registering channels: " + Arrays.toString(packet.data.toString(Charsets.UTF_8).split("\0")) );
            super.a(packet);
        } else if(packet.tag.equals(new MinecraftKey("unregister")))  {
            System.out.println("Client " + getPlayer().getName() + " unregistering channels: " + Arrays.toString(packet.data.toString(Charsets.UTF_8).split("\0")) );
            super.a(packet);
        }
        else {
            System.out.println(packet.tag);
            super.a(packet);
        }
    }


}

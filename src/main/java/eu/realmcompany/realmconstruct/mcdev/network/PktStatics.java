package eu.realmcompany.realmconstruct.mcdev.network;

import eu.realmcompany.realmconstruct.mcdev.network.client.RealmPlayerConnection;
import net.minecraft.server.v1_16_R1.*;
import org.bukkit.Particle;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

/**
 * Packet statics
 */
public class PktStatics {

    /**
     * Set's player connection to realmconnection
     * @param player Player
     */
    public static void setRealmConnection(@NotNull Player player) {
        EntityPlayer nmsPlayer = getNmsPlayer(player);
        nmsPlayer.playerConnection.networkManager.setPacketListener(new RealmPlayerConnection(
                nmsPlayer.server,
                nmsPlayer.playerConnection.networkManager,
                nmsPlayer));
    }
    /**
     * Set's player connection to realmconnection
     * @param player Player
     */
    public static RealmPlayerConnection getRealmConnection(@NotNull Player player) {
        return (RealmPlayerConnection) getNmsPlayer(player).playerConnection;
    }

    /**
     * Get nms player from Bukkit player
     * @param player Bukkit player
     * @return Nms Player
     */
    public static @NotNull EntityPlayer getNmsPlayer(@NotNull Player player) {
        return  ((CraftPlayer) player).getHandle();
    }


/**
 * Creates particle packet
 * @param particle         Particle type from net.minecraft.server.v1_16_R1.Particles
 * @param overrideLimiter  Override limiter
 * @param x                World pos X
 * @param y                World pos Y
 * @param z                World pos Z
 * @param offsetX          Offset of X
 * @param offsetY          Offset of Y
 * @param offsetZ          Offset of Z
 * @param speed            Speed of particle
 * @param count            Count of particles
 * @return packet
 */
public static PacketPlayOutWorldParticles makeParticlePacket(@NotNull ParticleType particle, boolean overrideLimiter, double x, double y, double z, float offsetX, float offsetY, float offsetZ, float speed, int count) {
    return new PacketPlayOutWorldParticles(particle, overrideLimiter, x, y, z, offsetX, offsetY, offsetZ, speed, count);
}

    /**
     * Creates particle packet
     * @param particle         Particle type from net.minecraft.server.v1_16_R1.Particles
     * @param overrideLimiter  Override limiter
     * @param loc              World pos vector
     * @param offset           Offset vector
     * @param speed            Speed of particle
     * @param count            Count of particles
     * @return packet
     */
    public static PacketPlayOutWorldParticles makeParticlePacket(@NotNull ParticleType particle, boolean overrideLimiter, Vector loc, @NotNull Vector offset, float speed, int count) {
        return makeParticlePacket(particle, overrideLimiter, loc.getX(), loc.getY(), loc.getZ(), (float) offset.getX(), (float) offset.getY(), (float) offset.getZ(), speed, count);
    }

    /**
     * Sends packet to player
     * @param player Player
     * @param packet Packet to send
     */
    public static void sendPacket(@NotNull Player player, @NotNull Packet<PacketListenerPlayOut> packet) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

}

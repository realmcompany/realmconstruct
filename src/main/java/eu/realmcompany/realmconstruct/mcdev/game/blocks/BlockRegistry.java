package eu.realmcompany.realmconstruct.mcdev.game.blocks;

import eu.realmcompany.realmconstruct.mcdev.game.blocks.model.KekBlock;
import net.minecraft.server.v1_16_R1.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

public class BlockRegistry {

    private static final Logger LOGGER = LogManager.getLogger();

    public static final Block KEK =
            injectToRegistry(new MinecraftKey("realmland", "kek"), new KekBlock(BlockBase.Info.a(Material.STONE).h().d(3.5F)));

    public static<T extends Block> Block injectToRegistry(@NotNull MinecraftKey key, @NotNull T block) {
        LOGGER.info("Register new block: {}", key.toString());
        return IRegistry.a(IRegistry.BLOCK, key, block);
    }

}

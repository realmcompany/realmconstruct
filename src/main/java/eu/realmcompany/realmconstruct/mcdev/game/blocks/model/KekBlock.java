package eu.realmcompany.realmconstruct.mcdev.game.blocks.model;

import eu.realmcompany.realmconstruct.mcdev.game.blocks.BlockRegistry;
import eu.realmcompany.realmconstruct.mcdev.game.blocks.TileRegistry;
import eu.realmcompany.realmconstruct.mcdev.game.blocks.tile.KekTile;
import net.minecraft.server.v1_16_R1.*;

public class KekBlock extends BlockSmoker {

    public KekBlock(Info var0) {
        super(var0);
    }


    @Override
    public TileEntity createTile(IBlockAccess var0) {
        return new KekTile();
    }

    @Override
    public float getFrictionFactor() {
        return 0;
    }
}


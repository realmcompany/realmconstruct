package eu.realmcompany.realmconstruct.mcdev.game.entites;

import lombok.Getter;
import net.minecraft.server.v1_16_R1.*;
import org.jetbrains.annotations.NotNull;

public class EntityDepot {

    // Factory, Entity type, serialize, summon, fireImmune, canSpawnFarFromPlayer
    public final EntityTypes<EntityShulker> HIGHLIGHT =
            injectToRegistry("highlight",
                    EntityBuilder.of(EntityShulker::new, EnumCreatureType.AMBIENT).sized(1f, 1f).noSave());


    //TODO: Figure out podla coho su bindovane IDcka entit na nazvy
    // check registry
    public <T extends Entity>EntityTypes<T> injectToRegistry(@NotNull String namespace, @NotNull EntityBuilder<T> entity) {
        IRegistry.ENTITY_TYPE.a(59, ResourceKey.a(IRegistry.n, new MinecraftKey(namespace)), entity.build(namespace));
        return IRegistry.a(IRegistry.ENTITY_TYPE, namespace, entity.build(namespace));
    }


    /**
     * Builds entity
     * @param <T> Entity type
     */
    public static class EntityBuilder<T extends Entity> {

        @Getter
        private EntityTypes.Builder<T> nmsFactory = null;


        private EntityBuilder(@NotNull EntityFactory<T> factory, @NotNull EnumCreatureType creatureType) {
            this.nmsFactory = EntityTypes.Builder.a((EntityTypes.b<T>) factory::create, creatureType);
        }

        /**
         * Creates new EntityBuilder with specified {@link EntityFactory}.
         * @param factory EntityFactory
         * @param creatureType CreatureType
         * @param <T> Type of entity
         * @return new EntityBuilder with specified factory
         */
        public static <T extends Entity>EntityBuilder<T> of(@NotNull EntityFactory<T> factory, @NotNull EnumCreatureType creatureType) {
            return new EntityBuilder<T>(factory, creatureType);
        }

        /**
         * Creates new EntityBuilder without {@link EntityFactory}.
         * @param creatureType CreatureType
         * @param <T> Type of Entity
         * @return new EntityBuilder
         */
        public static <T extends Entity>EntityBuilder<T> createNothing(@NotNull EnumCreatureType creatureType) {
            return new EntityBuilder<T>((entity, world) -> null, creatureType);
        }

        public EntityBuilder<T> sized(float width, float height) {
            this.nmsFactory.a(width, height);
            return this;
        }

        public EntityBuilder<T> noSummon() {
            this.nmsFactory.a();
            return this;
        }

        public EntityBuilder<T> noSave() {
            this.nmsFactory.b();
            return this;
        }

        public EntityBuilder<T> fireImmune() {
            this.nmsFactory.c();
            return this;
        }

        public EntityBuilder<T> canSpawnFarFromPlayer() {
            this.nmsFactory.d();
            return this;
        }

        public EntityTypes<T> build(@NotNull String key) {
            return this.nmsFactory.a(key);
        }
    }

    /**
     * Constructs entity
     * @param <T> Type of entity
     */
    public interface EntityFactory<T extends Entity> {
        T create(EntityTypes<T> var1, World var2);
    }

}

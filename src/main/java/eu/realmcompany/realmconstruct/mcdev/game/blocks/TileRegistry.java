package eu.realmcompany.realmconstruct.mcdev.game.blocks;

import com.google.common.collect.ImmutableSet;
import com.mojang.datafixers.types.Type;
import eu.realmcompany.realmconstruct.mcdev.game.blocks.tile.KekTile;
import net.minecraft.server.v1_16_R1.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class TileRegistry<T extends TileEntity> {

    private static final Logger LOGGER = LogManager.getLogger();

    public static final TileEntityTypes<KekTile> KEK =
            injectToRegistry(new MinecraftKey("realmland", "kek"), Builder.of(KekTile::new, BlockRegistry.KEK));


    public static<T extends TileEntity> TileEntityTypes<T> injectToRegistry(@NotNull MinecraftKey key, @NotNull Builder<T> builder) {
        LOGGER.info("Register new tile block: {} bound to {}", key.toString(), builder.validBlocks.stream().map(Block::i).collect(Collectors.toList()));

        Type<?> var2 = SystemUtils.a(DataConverterTypes.BLOCK_ENTITY, key.toString());
        return IRegistry.a(IRegistry.BLOCK_ENTITY_TYPE, key, builder.build(var2));
    }

    /**
     * TileEntity builder
     * @param <T> Type of TileEntity
     */
    public static class Builder<T extends TileEntity> {
        private final Supplier<? extends T> factory;
        private final Set<Block> validBlocks;

        private Builder(Supplier<? extends T> factory, Set<Block> validBlocks) {
            this.factory = factory;
            this.validBlocks = validBlocks;
        }

        public static <T extends TileEntity> Builder<T> of(Supplier<? extends T> var0, Block... var1) {
            return new Builder<>(var0, ImmutableSet.copyOf(var1));
        }

        public TileEntityTypes<T> build(Type<?> var0) {
            return new TileEntityTypes(this.factory, this.validBlocks, var0);
        }
    }

}

package eu.realmcompany.realmconstruct.resources.store;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.resources.store.model.IStore;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class JsonStorage implements IStore {

    @Getter
    private final BukkitPlugin plugin;

    @Getter
    private final File file;

    @Getter
    private JsonObject data;

    @Getter
    private final boolean loadDefaulted;

    /**
     * Loads data from file
     *
     * @param plugin        Plugin instance
     * @param name          File name (without file suffix)
     * @param loadDefaulted Whether to laod default file or not,
     */
    public JsonStorage(@NotNull BukkitPlugin plugin, @NotNull String name, boolean loadDefaulted) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), name + ".json");
        this.loadDefaulted = loadDefaulted;
        if (!file.exists()) {
            if (loadDefaulted)
                loadDefault();
            save();
        }
        load();
    }

    @Override
    public void save() {
        this.file.getParentFile().mkdirs();
        try {
            if (!this.file.exists())
                if (!this.file.createNewFile()) {
                    this.plugin.getLogger().warning("File '" + this.file.getName() + "' not created.");
                    return;
                }

            try (FileWriter writer = new FileWriter(this.file)) {
                writer.write(this.data.toString());
            }
        } catch (IOException e) {
            this.plugin.getLogger().warning("Failed to save data '" + this.file.getName() + "' because: " + e);
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void load() {
        if (this.file.exists()) {
            try (FileReader reader = new FileReader(this.file)) {
                this.data = new JsonParser().parse(reader).getAsJsonObject();
            } catch (IOException e) {
                this.plugin.getLogger().warning("Failed to load data '" + this.file.getName() + "' because: " + e);
                e.printStackTrace(System.err);
            }
        } else
            this.data = new JsonObject();
    }

    @Override
    public void loadDefault() {
        if (this.file.exists()) {
            try (InputStreamReader reader =
                         new InputStreamReader(this.plugin.getResource(this.file.getName()), StandardCharsets.UTF_8)) {
                this.data = new JsonParser().parse(reader).getAsJsonObject();
            } catch (IOException e) {
                this.plugin.getLogger().warning("Failed to load default data '" + this.file.getName() + "' because: " + e);
                e.printStackTrace(System.err);
            }
        } else
            this.data = new JsonObject();
    }
}

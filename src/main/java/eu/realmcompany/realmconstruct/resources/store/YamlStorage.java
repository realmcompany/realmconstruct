package eu.realmcompany.realmconstruct.resources.store;

import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.resources.store.model.IStore;
import lombok.Getter;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class YamlStorage implements IStore {

    @Getter
    private final BukkitPlugin plugin;

    @Getter
    private final File file;
    @Getter
    private final FileConfiguration configuration;

    @Getter
    private final boolean loadDefaulted;

    /**
     * Loads configuration from file
     *
     * @param plugin        Plugin instance
     * @param name          File name (without file suffix)
     * @param loadDefaulted Whether to laod default file or not,
     */
    public YamlStorage(@NotNull BukkitPlugin plugin, @NotNull String name, boolean loadDefaulted) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), name + ".yml");
        this.configuration = new YamlConfiguration();
        this.loadDefaulted = loadDefaulted;
        if (!file.exists()) {
            if (loadDefaulted)
                loadDefault();
            save();
        }
        load();
    }

    @Override
    public void save() {
        try {
            this.configuration.save(this.file);
        } catch (IOException e) {
            this.plugin.getLogger().warning("Failed to save configuration '" + this.file.getName() + "' because: " + e);
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void load() {
        try {
            this.configuration.load(this.file);
        } catch (IOException e) {
            this.plugin.getLogger().warning("Failed to load configuration '" + this.file.getName() + "': " + e);
            e.printStackTrace(System.err);
        } catch (InvalidConfigurationException e) {
            this.plugin.getLogger().warning("Failed to load configuration '" + this.file.getName() + "', because of bad format: " + e);
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void loadDefault() {
        try {
            // todo use classic IO, because this ignores comments
            this.configuration.load(new InputStreamReader(this.plugin.getResource(this.file.getName()), StandardCharsets.UTF_8));
        } catch (IOException e) {
            this.plugin.getLogger().warning("Failed to load default configuration '" + this.file.getName() + "': " + e);
            e.printStackTrace(System.err);
        } catch (InvalidConfigurationException e) {
            this.plugin.getLogger().warning("Failed to load default configuration '" + this.file.getName() + "', because of bad format: " + e);
            e.printStackTrace(System.err);
        }
    }
}

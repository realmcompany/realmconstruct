package eu.realmcompany.realmconstruct.resources.store.model;

/**
 * Represents writable file, with special format
 */
public interface IStore {

    void save();

    void load();
    void loadDefault();

}

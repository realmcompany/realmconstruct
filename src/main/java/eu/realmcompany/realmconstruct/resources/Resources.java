package eu.realmcompany.realmconstruct.resources;

import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.abstraction.AComponent;
import eu.realmcompany.realmconstruct.resources.store.JsonStorage;
import eu.realmcompany.realmconstruct.resources.store.YamlStorage;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public class Resources extends AComponent {

    @Getter
    private YamlStorage pluginConfiguration;

    public Resources(@NotNull BukkitPlugin instance) {
        super(instance);
        this.pluginConfiguration = new YamlStorage(instance, "configuration", true);
    }

    @Override
    public void initialize() throws Exception {

    }

    @Override
    public void terminate() throws Exception {

    }
}

package eu.realmcompany.realmconstruct.modx.botanik;

import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.abstraction.AComponent;
import org.jetbrains.annotations.NotNull;

public class Botanik extends AComponent {

    public Botanik(@NotNull BukkitPlugin instance) {
        super(instance);
    }

    @Override
    public void initialize() throws Exception {

    }

    @Override
    public void terminate() throws Exception {

    }
}

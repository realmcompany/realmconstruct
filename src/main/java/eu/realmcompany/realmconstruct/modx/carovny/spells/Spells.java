package eu.realmcompany.realmconstruct.modx.carovny.spells;

import eu.realmcompany.realmconstruct.modx.carovny.spells.model.Spell;
import eu.realmcompany.realmconstruct.modx.carovny.spells.model.patterns.SpellPattern;
import eu.realmcompany.realmconstruct.modx.carovny.spells.model.spells.LightningSpell;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public class Spells<T extends Spell> {

    public static final Spells<LightningSpell> LIGHTNING_SPELL =
            construct(SpellPattern.fromFile(new File("C:/Users/maros/Downloads/spell_pattern.png")), LightningSpell::new);

    @Getter
    private SpellPattern pattern;
    @Getter
    private Factory<T>   factory;

    /**
     * Default constructor
     * @param pattern Pattern of spell
     * @param factory Factory of spell
     */
    private Spells(@NotNull SpellPattern pattern, @NotNull Factory<T> factory) {
        this.pattern = pattern;
        this.factory = factory;
    }

    private Spells() {
    }

    private static <T extends Spell>Spells<T> construct(@NotNull SpellPattern pattern, @NotNull Factory<T> factory) {
        return new Spells<>(pattern, factory);
    }

    /**
     * Creates spell
     * @param player Owner of spell
     * @return Spell
     */
    public T createSpell(@NotNull Player player) {
        return this.getFactory().make(this.pattern, player);
    }

    /**
     * Factory
     * @param <T> Type of Spell
     */
    public static interface Factory<T extends Spell> {
        T make(@NotNull SpellPattern pattern, @NotNull Player summoner);
    }

}

package eu.realmcompany.realmconstruct.modx.carovny.spells.model.patterns;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;

public class SpellPattern {

    private static Logger logger = LogManager.getLogger("SpellPattern");

    @Getter
    @Setter(AccessLevel.PROTECTED)
    private int resolution;
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private byte[] pattern;

    /**
     * Default constructor
     * @param resolution Resolution
     * @param pattern    Pattern
     */
    public SpellPattern(int resolution, byte[] pattern) {
        this.resolution = resolution;
        this.pattern = pattern;
    }

    public static SpellPattern fromFile(@NotNull File imageFile) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(imageFile);
            if(image.getHeight() != image.getWidth()) {
                logger.error("Height must be equal to width: " + imageFile.getPath());
                return null;
            }

            DataBufferByte data = ((DataBufferByte) image.getRaster().getDataBuffer());

            return new SpellPattern(image.getHeight(), data.getData());
        } catch (IOException e) {
            logger.error("Couldn't load image from file: " + imageFile.getPath());
            e.printStackTrace();
        }
        return null;
    }
}

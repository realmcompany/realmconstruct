package eu.realmcompany.realmconstruct.modx.carovny.spells.model.spells;

import eu.realmcompany.realmconstruct.modx.carovny.spells.model.Spell;
import eu.realmcompany.realmconstruct.modx.carovny.spells.model.patterns.SpellPattern;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class LightningSpell extends Spell {

    public LightningSpell(@NotNull SpellPattern pattern, @NotNull Player summoner) {
        super(pattern, summoner);
    }

    @Override
    public void summonSpell() {
        startRendering();
    }

    @Override
    public void performSpell() {
        stopRendering();

        System.out.println("perform");

        Block block = getSummoner().getTargetBlock(5);
        System.out.println("tes");
        if(block != null) {
            System.out.println(block.getLocation().toVector());
            getSummoner().getWorld().strikeLightning(block.getLocation());
            System.out.println("aaaaaaaaaaaa");
        }
        else {
            getSummoner().getWorld().createExplosion(getSummoner().getLocation(), 10, false);
            System.out.println(":(");
        }
    }
}

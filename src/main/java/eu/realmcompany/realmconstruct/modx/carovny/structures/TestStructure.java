package eu.realmcompany.realmconstruct.modx.carovny.structures;

import com.mojang.serialization.Codec;
import net.minecraft.server.v1_16_R1.*;

public class TestStructure extends StructureGenerator<WorldGenFeatureEmptyConfiguration> {

    public TestStructure(Codec<WorldGenFeatureEmptyConfiguration> codec) {
        super(codec);
    }

    @Override
    public StructureGenerator.a<WorldGenFeatureEmptyConfiguration> a() {
        return null;
    }
}

package eu.realmcompany.realmconstruct.modx.carovny.spells.model;

import eu.realmcompany.realmconstruct.modx.carovny.spells.model.patterns.SpellPattern;
import eu.realmcompany.realmconstruct.mcdev.math.Mth;
import eu.realmcompany.realmconstruct.mcdev.network.PktStatics;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_16_R1.PacketPlayOutWorldParticles;
import net.minecraft.server.v1_16_R1.Particles;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;

public abstract class Spell implements Listener {

    private Logger logger = LogManager.getLogger("Spell");

    @Getter
    private @NotNull SpellPattern pattern;

    @Getter
    @Setter(AccessLevel.PROTECTED)
    private int speed = 0;
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private int count = 1;

    /**
     * If true, the spell will be visible to all players
     */
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private boolean isVisible;

    private boolean shouldRender = false;
    private double renderTimes[] = new double[5];

    /**
     * Distance between player eyes and rendered pattern
     */
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private double distance = 3d;

    /**
     * Distance between each pixel (particle)
     */
    @Getter
    @Setter(AccessLevel.PROTECTED)
    private double offset = 3.5d;

    @Getter
    private @NotNull Player summoner;

    public Spell(@NotNull SpellPattern pattern, @NotNull Player summoner) {
        this.pattern = pattern;
        this.summoner = summoner;
    }

    /**
     * Called upon summoning of spell.
     */
    public abstract void summonSpell();

    /**
     * Called upon activation of spell.
     *
     */
    public abstract void performSpell();

    /**
     * Starts rendering spell in front of a player.
     *
     */
    public void startRendering() {
        this.shouldRender = true;
        render();
    }

    /**
     * Stops rendering spell
     */
    public void stopRendering() {
        this.shouldRender = false;
    }

    public void render() {
        Executors.newSingleThreadExecutor().submit(() -> {
            while (this.shouldRender && summoner.isOnline()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    logger.error("Failed to render spell for player {}", summoner.getName());
                    this.shouldRender = false;
                    e.printStackTrace(System.err);
                }

                final Location location = summoner.getLocation();
                final Location eyeLocation = summoner.getEyeLocation();

                byte[] data = pattern.getPattern();
                int localY = 0;
                int localX = -1;
                for (int i = data.length - 1; i >= 0; i -= 4) {
                    localX++;
                    if (localX == pattern.getResolution()) {
                        localY++;
                        localX = 0;
                    }

                    int blue = data[i];
                    int green = data[i - 1];
                    int red = data[i - 2];
                    int transparency = data[i - 3];


                    Vector plane = new Vector(localX / getOffset(), localY / getOffset(), getDistance());
                    double magnitude = Math.sqrt(Math.pow(plane.getX(), 2) + Math.pow(plane.getY(), 2) + Math.pow(plane.getZ(), 2));

                    Vector localPos = Mth.sphericalToCartesian(
                            magnitude,
                            Mth.yawToDeg(location.getYaw()) - localX * getOffset() + 22.5,
                            Mth.pitchToDeg(location.getPitch()) - localY * getOffset() + 22.5);


                    if (blue == 0xffffffff && green == 0xffffffff && red == 0xffffffff && transparency == 0xffffffff) {
                        PacketPlayOutWorldParticles packet = PktStatics.makeParticlePacket(Particles.CRIT, false,
                                localPos.add(eyeLocation.toVector()), new Vector(), this.speed, this.count);
                        if (this.isVisible())
                            Bukkit.getOnlinePlayers().forEach(serverPlayer -> {
                                PktStatics.sendPacket(serverPlayer, packet);
                            });
                        else
                            PktStatics.sendPacket(summoner, packet);
                    }
                }
            }
        });

    }


    @Override
    protected void finalize() throws Throwable {
        System.out.println("papa");
    }
}

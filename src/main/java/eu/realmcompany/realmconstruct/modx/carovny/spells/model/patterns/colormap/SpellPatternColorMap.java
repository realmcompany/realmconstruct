package eu.realmcompany.realmconstruct.modx.carovny.spells.model.patterns.colormap;

import lombok.Getter;
import net.minecraft.server.v1_16_R1.ParticleType;
import net.minecraft.server.v1_16_R1.Particles;

public class SpellPatternColorMap {

    public static final SpellPatternColorMap WHITE = new SpellPatternColorMap(0xffffffff,0xffffffff, 0xffffffff, Particles.CRIT);
    public static final SpellPatternColorMap BLACK = new SpellPatternColorMap(0x00000000,0x00000000, 0x00000000, Particles.SMOKE);

    @Getter
    private final long red;
    @Getter
    private final long green;
    @Getter
    private final long blue;

    @Getter
    private final ParticleType particleType;

    /**
     * Default constructor
     *
     * @param red          Red channel
     * @param green        Green channel
     * @param blue         Blue channel
     * @param particleType Particle type
     */
    public SpellPatternColorMap(long red, long green, long blue, ParticleType particleType) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.particleType = particleType;
    }

    /**
     * Checks if specified color matches this color exactly
     *
     * @param red   Red
     * @param green Green
     * @param blue  Blue
     * @return Boolean
     */
    public boolean matchesExact(long red, long green, long blue) {
        return (this.red == red && this.green == green && this.blue == blue);
    }

    /**
     * Checks if specified color matches this color
     *
     * @param red   Red
     * @param green Green
     * @param blue  Blue
     * @return Boolean
     */
    public boolean matches(long red, long green, long blue) {
        return (this.red - red) < 1000 && (this.red - red) > -1000 &&
                (this.green - red) < 1000 && (this.green - red) > -1000 &&
                (this.blue - red) < 1000 && (this.blue - red) > -1000;
    }
}

package eu.realmcompany.realmconstruct.modx.carovny;

import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.abstraction.AComponent;
import org.jetbrains.annotations.NotNull;

public class Carovny extends AComponent {

    public Carovny(@NotNull BukkitPlugin instance) {
        super(instance);
    }

    @Override
    public void initialize() throws Exception {

    }

    @Override
    public void terminate() throws Exception {

    }
}

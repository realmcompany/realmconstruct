package eu.realmcompany.realmconstruct;

import eu.realmcompany.realmconstruct.abstraction.AComponent;
import eu.realmcompany.realmconstruct.audit.Logger;
import eu.realmcompany.realmconstruct.modx.carovny.spells.Spells;
import eu.realmcompany.realmconstruct.modx.carovny.spells.model.spells.LightningSpell;
import eu.realmcompany.realmconstruct.mcdev.math.Mth;
import eu.realmcompany.realmconstruct.mcdev.network.PktStatics;
import eu.realmcompany.realmconstruct.mcdev.network.server.RealmServerConnection;
import eu.realmcompany.realmconstruct.network.remote.RealmRemote;
import eu.realmcompany.realmconstruct.resources.Resources;
import eu.realmcompany.realmconstruct.services.RealmService;
import eu.realmcompany.realmconstruct.statics.WorldStatics;
import lombok.Getter;
import net.minecraft.server.v1_16_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_16_R1.CraftServer;
import org.bukkit.craftbukkit.v1_16_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Bukkit plugin
 */
public class BukkitPlugin extends JavaPlugin {

    @Getter
    private Logger realmLogger;

    @Getter
    private Resources resources;

    @Getter
    private RealmServerConnection serverConnection;

    @Getter
    private RealmService realmService;

    @Getter
    private RealmRemote realmRemote;


    @Override
    public void onLoad() {
        this.realmLogger = new Logger();

        try {
            serverConnection = new RealmServerConnection(((CraftServer) Bukkit.getServer()).getServer());
        } catch (Exception x) {
            getRealmLogger().error("Failed to construct RealmServerConnection");
        }

        this.resources = constructComponent("Resources", Resources.class);
        this.realmService = constructComponent("RealmService", RealmService.class);
        this.realmRemote = constructComponent("RealmRemote", RealmRemote.class);


        this.getServer().getCommandMap().register("realmland", new Command("coolrain") {
            @Override
            public boolean execute(@NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] strings) {

                if (strings.length < 1) {
                    commandSender.sendMessage("§cMissing argument: §f[reset, <float value>]");
                    return true;
                }
                String argument = strings[0];

                try {
                    PacketPlayOutGameStateChange.a event = PacketPlayOutGameStateChange.h;
                    float param = Float.parseFloat(argument);
                    if (strings.length > 1 && strings[1].equalsIgnoreCase("8")) {
                        event = PacketPlayOutGameStateChange.i;
                        commandSender.sendMessage("§aEvent id 8");
                    }

                    final PacketPlayOutGameStateChange.a finalEvent = event;
                    Bukkit.getOnlinePlayers().stream().map(player -> ((CraftPlayer) player).getHandle().playerConnection).forEach(con -> {
                        con.sendPacket(new PacketPlayOutGameStateChange(finalEvent, param));
                    });

                } catch (NumberFormatException x) {
                    if (argument.equalsIgnoreCase("reset")) {
                        Bukkit.getOnlinePlayers().stream().map(player -> ((CraftPlayer) player).getHandle().playerConnection).forEach(con -> {
                            con.sendPacket(new PacketPlayOutGameStateChange(PacketPlayOutGameStateChange.h, 0f));
                            con.sendPacket(new PacketPlayOutGameStateChange(PacketPlayOutGameStateChange.i, 0f));
                        });
                    } else
                        commandSender.sendMessage("§cUnknown subcommand:§f " + argument);
                }

                return true;
            }
        });
        this.getServer().getCommandMap().register("realmland", new Command("ping") {
            @Override
            public boolean execute(@NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] strings) {

                if (strings.length < 1) {
                    commandSender.sendMessage("§7Your ping: §ef" + PktStatics.getNmsPlayer((Player) commandSender).ping + "ms");
                    return true;
                }
                String targetName = strings[0];
                Player target = Bukkit.getPlayer(targetName);
                if(target == null)
                    commandSender.sendMessage("§cNope. Not a player.");
                else
                    commandSender.sendMessage("§7" + target.getName() +"'s ping: §ef" + PktStatics.getNmsPlayer(target).ping + "ms");
                return true;
            }
        });
        this.getServer().getCommandMap().register("realmland", new Command("spell") {
            @Override
            public boolean execute(@NotNull CommandSender commandSender, @NotNull String s, @NotNull String[] strings) {

                Player player = (Player) commandSender;
                if (strings.length > 0) {
                    if (strings[0].equalsIgnoreCase("smoke")) {
                        Executors.newSingleThreadExecutor().submit(() -> {
                            while (player.isOnline()) {
                                try {
                                    Thread.sleep(100);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                PktStatics.sendPacket(player, PktStatics.makeParticlePacket(Particles.SMOKE, false, player.getEyeLocation().add(
                                        Mth.sphericalToCartesian(2,
                                                Mth.yawToDeg(player.getLocation().getYaw()), Mth.pitchToDeg(player.getLocation().getPitch()))).toVector(), new Vector(), 0, 5));
                            }
                        });
                        return true;
                    }
                    if (strings[0].equalsIgnoreCase("circle")) {
                        Executors.newSingleThreadExecutor().submit(() -> {
                            for (int i = 0; i < 360; i++) {
                                Vector base = player.getLocation().toVector();
                                try {
                                    Thread.sleep(50);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                WorldStatics.showHologram(player, base.add(Mth.polarToCartesian(2, i)), "" + i, 50, TimeUnit.MILLISECONDS);
                            }
                        });
                    }

                    if(strings[0].equalsIgnoreCase("lightning")) {
                        LightningSpell spell = Spells.LIGHTNING_SPELL.createSpell(player);
                        spell.summonSpell();

                        Executors.newSingleThreadExecutor().submit(() -> {
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            Bukkit.getScheduler().runTask(getRealmService().getPlugin(), spell::stopRendering);
                        });
                    }
                    return true;
                }

                Vector direction = Mth.sphericalToCartesian(1d, Mth.yawToDeg(player.getLocation().getYaw()), Mth.pitchToDeg(player.getLocation().getPitch()));

                for(int i = 1; i < 20; i++) {
                    Vector worldVector = direction.clone().multiply(i).add(player.getEyeLocation().toVector());
                    Block block = player.getWorld().getBlockAt(worldVector.toLocation(player.getWorld()));



                }

                return true;
            }
        });

        Bukkit.getServer().getCommandMap().register("realmland", new Command("test") {
            @Override
            public boolean execute(@NotNull CommandSender commandSender, @NotNull String s,  @NotNull String[] args) {
                org.bukkit.World bukkitWorld = getServer().getWorld("world");
                Player player = (Player) commandSender;
                Location loc = player.getLocation();
                if(bukkitWorld != null) {
                    World world = ((CraftWorld) bukkitWorld).getHandle().getMinecraftWorld();
                    Chunk c = world.getChunkIfLoaded(new BlockPosition(player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()));
                    BiomeBase base = c.getBiomeIndex().getBiome(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());

                    try {
                        Field skyColorField = base.getClass().getSuperclass().getDeclaredField("t");
                        skyColorField.setAccessible(true);
                        skyColorField.set(base, Mth.rgbToInt(255, 0, 0));
                        System.out.println(skyColorField.get(base));
                    } catch (NoSuchFieldException | IllegalAccessException e) {
                        e.printStackTrace();
                    }
                } else
                    System.out.println("world is null");
                return true;
            }
        });

    }

    @Override
    public void onEnable() {
        serverConnection.acceptConnections();

        initializeComponent("Resources", this.resources);
        initializeComponent("RealmService", this.realmService);
        initializeComponent("RealmRemote", this.realmRemote);



    }

    @Override
    public void onDisable() {
        terminateComponent("RealmRemote", this.realmRemote);
        terminateComponent("RealmService", this.realmService);
        terminateComponent("Resources", this.resources);
    }

    /**
     * Constructs component
     *
     * @param name   Name of component
     * @param tClass Component class
     * @param <T>    Type of component
     * @return Constructed component
     */
    private <T extends AComponent> T constructComponent(@NotNull String name, @NotNull Class<T> tClass) {
        T instance;
        try {
            instance = tClass.getConstructor(BukkitPlugin.class).newInstance(this);
            getRealmLogger().info("Constructed component %s", name);
            return instance;
        } catch (InvocationTargetException e) {
            getRealmLogger().error("Failed to construct component %s because it threw an exception.", e.getTargetException(), name);
        } catch (Exception e) {
            getRealmLogger().error("Failed to construct component %s", e, name);
        }
        return null;
    }

    /**
     * Initializes component
     *
     * @param name      Name of component
     * @param component Component instance
     * @param <T>       Type of component
     * @return True if no error occurred while initializing
     */
    private <T extends AComponent> boolean initializeComponent(@NotNull String name, @Nullable T component) {
        if (component == null)
            return false;
        try {
            component.initialize();
            getRealmLogger().info("Initialized component %s", name);
            return true;
        } catch (Exception e) {
            getRealmLogger().error("Failed to initialize component %s", e, name);
        }
        return false;
    }

    /**
     * Terminates component
     *
     * @param name      Name of component
     * @param component Component instance
     * @param <T>       Type of component
     * @return True if no error occurred while initializing
     */
    private <T extends AComponent> boolean terminateComponent(@NotNull String name, @Nullable T component) {
        if (component == null)
            return false;
        try {
            component.terminate();
            getRealmLogger().info("Terminated component %s", name);
            return true;
        } catch (Exception e) {
            getRealmLogger().error("Failed to terminate component %s", e, name);
        }
        return false;
    }
}

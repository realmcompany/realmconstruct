package eu.realmcompany.realmconstruct.abstraction;

import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.audit.Logger;
import lombok.Getter;
import eu.realmcompany.realmconstruct.services.RealmService;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.jetbrains.annotations.NotNull;

/**
 * Service
 */
public abstract class AService implements Listener {

    @Getter
    private String serviceName = this.getClass().getSimpleName();

    @Getter
    private final BukkitPlugin plugin;
    @Getter
    private final RealmService instance;

    @Getter
    private final Logger logger;


    /**
     * Service constructor
     * @param instance Service instance
     */
    public AService(@NotNull RealmService instance) {
        this.plugin = instance.getPlugin();
        this.instance = instance;
        this.logger = instance.getLogger();
    }

    /**
     * Called on service initialization
     * @throws Exception Thrown when Service couldn't handle Exception.
     */
    abstract public void initialize() throws Exception;

    /**
     * Called on service termination
     * @throws Exception Thrown when Service couldn't handle Exception.
     */
    abstract public void terminate()  throws Exception;

    /**
     * Sexier way of registering listeners
     */
    public void registerAsListener() {
        Bukkit.getPluginManager().registerEvents((Listener) this, getPlugin());
    }

    /**
     * Sexier way of unregistering listeners
     */
    public void unregisterAsListener() {
        HandlerList.unregisterAll(this);
    }



    /**
     * Represents child of Service
     * @param <T> Type of Parent
     */
    public static abstract class Child<T extends AService> extends AService {

        @Getter
        private final T parent;

        public Child(@NotNull T parent) {
            super(parent.getInstance());
            this.parent = parent;
        }
    }

}

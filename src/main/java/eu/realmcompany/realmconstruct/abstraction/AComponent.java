package eu.realmcompany.realmconstruct.abstraction;

import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.audit.Logger;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public abstract class AComponent {

    @Getter
    private final BukkitPlugin plugin;

    @Getter
    private Logger logger;

    /**
     * Default component constructor
     * @param instance Plugin instance
     */
    public AComponent(@NotNull BukkitPlugin instance) {
        this.plugin = instance;
        this.logger = instance.getRealmLogger();
    }

    /**
     * Called on component initialization
     * @throws Exception Thrown when Component couldn't handle Exception.
     */
    abstract public void initialize() throws Exception;

    /**
     * Called on component termination
     * @throws Exception Thrown when Component couldn't handle Exception.
     */
    abstract public void terminate()  throws Exception;
}

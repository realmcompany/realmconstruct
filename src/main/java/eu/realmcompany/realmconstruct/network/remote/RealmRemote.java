package eu.realmcompany.realmconstruct.network.remote;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import eu.realmcompany.realmconstruct.BukkitPlugin;
import eu.realmcompany.realmconstruct.abstraction.AComponent;
import eu.realmcompany.realmconstruct.mcdev.network.client.RealmPlayerConnection;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_16_R1.entity.CraftPlayer;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;

/**
 * Communicates with remote hosts
 */
public class RealmRemote extends AComponent {

    @Getter
    private HttpServer publicRemote;

    @Getter
    private HttpServer privateRemote;

    /**
     * Default constructor
     * @param instance Instance
     */
    public RealmRemote(@NotNull BukkitPlugin instance) {
        super(instance);

        try {
            int port = this.getPlugin().getResources()
                    .getPluginConfiguration().getConfiguration().getInt("network.remote.public.listening-port", 26090);
            int backlog = this.getPlugin().getResources()
                    .getPluginConfiguration().getConfiguration().getInt("network.remote.public.backlog", 5);

            this.publicRemote = HttpServer.create(
                    new InetSocketAddress(port), backlog);

            this.publicRemote.createContext("/status/", new HttpHandler() {
                @Override
                public void handle(HttpExchange exchange) throws IOException {
                    JsonObject data = new JsonObject();
                    data.addProperty("tps", Arrays.toString(Bukkit.getServer().getTPS()));
                    data.addProperty("version", Bukkit.getServer().getVersion());
                    data.addProperty("player_count", Bukkit.getServer().getOnlinePlayers().size());
                    JsonArray playerData = new JsonArray();
                    Bukkit.getOnlinePlayers().forEach(player -> {
                        JsonObject info = new JsonObject();
                        info.addProperty("name" , player.getName());
                        info.addProperty("main_hand", player.getMainHand().name());
                        info.addProperty("view_distance" , player.getClientViewDistance());
                        info.addProperty("is_modded" , ((RealmPlayerConnection) ((CraftPlayer) player).getHandle().playerConnection).isModded());

                        playerData.add(info);
                    });
                    data.add("players", playerData);

                    String bin = data.toString();

                    exchange.sendResponseHeaders(200, bin.getBytes().length);
                    exchange.getResponseBody().write(bin.getBytes());
                    exchange.close();
                }
            });
        } catch (IOException e) {
            this.getLogger().error("Failed to construct public HttpServer", e);
        }

    }

    @Override
    public void initialize() throws Exception {
        this.publicRemote.start();
        this.getLogger().info("Starting public remote on port: " + this.publicRemote.getAddress().getPort());
    }

    @Override
    public void terminate() throws Exception {
        this.getLogger().info("Stopping public remote");
        this.publicRemote.stop(0);
    }
}

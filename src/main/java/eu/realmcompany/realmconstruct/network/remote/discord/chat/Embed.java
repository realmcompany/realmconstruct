package eu.realmcompany.realmconstruct.network.remote.discord.chat;

import org.jetbrains.annotations.NotNull;

public class Embed {

    private @NotNull String title;
    private @NotNull Type   type;
    private @NotNull String description;
    private @NotNull String url;
    private @NotNull long   timestamp;
    private @NotNull int    color;


    public enum  Type {
        RICH, IMAGE, VIDEO, GIFV, ARTICLE, LINK
    }

}

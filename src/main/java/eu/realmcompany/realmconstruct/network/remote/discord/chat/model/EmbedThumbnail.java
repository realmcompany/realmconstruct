package eu.realmcompany.realmconstruct.network.remote.discord.chat.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

public class EmbedThumbnail {

    @Getter
    private @NotNull String url;

    @Getter
    private @NotNull String proxyUrl;

    


}

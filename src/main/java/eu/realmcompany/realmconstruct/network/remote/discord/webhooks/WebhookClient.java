package eu.realmcompany.realmconstruct.network.remote.discord.webhooks;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class WebhookClient {

    @Getter
    private @NotNull String clientName;
    @Getter
    private @Nullable String clientAvatar = null;

    @Getter
    private @NotNull String token;
    @Getter
    private @NotNull String channel;

    private WebhookClient() {

    }


    public static class Builder {

        private WebhookClient client;

        private Builder() {
            this.client = new WebhookClient();
        }

        public static Builder make(@NotNull String name, @Nullable String avatar) {
            Builder builder = new Builder();
            builder.client.clientName = name;
            builder.client.clientAvatar = avatar;

            return builder;
        }

        public Builder withToken(@NotNull String token) {
            this.client.token = token;
            return this;
        }

        public Builder toChannel(@NotNull String channel) {
            this.client.channel = channel;
            return this;
        }

        public WebhookClient build() {



            return client;
        }

    }


}
